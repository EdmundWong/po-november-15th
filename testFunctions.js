function testGetVowelSubstrings(){
    console.log(getVowelSubstrings("apple"));
    console.log(getVowelSubstrings("aviation"));
    console.log(getVowelSubstrings("dictionary"));
    console.log(getVowelSubstrings("wormhole"));
    console.log(getVowelSubstrings("motor"));
}

function testGetConsonantSubstrings(){
    console.log(getConsonantSubstrings("apple"));
    console.log(getConsonantSubstrings("aviation"));
    console.log(getConsonantSubstrings("dictionary"));
    console.log(getConsonantSubstrings("wormhole"));
    console.log(getConsonantSubstrings("motor"));
}