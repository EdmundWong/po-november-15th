/*1) Write two functions:
- One to retrieve all unique substrings that start and end with a vowel.
- One to retrieve all unique substrings that start and end with a consonant.
The resulting array should be sorted in lexicographic ascending order (same order as a dictionary).
*/
function getVowelSubstrings(str){
    let ret = [];
    for(let i=0;i<str.length;i++){
        if(isVowel(str.charAt(i))){
            for(let j=str.length-1;j>=i;j--){
                if(isVowel(str.charAt(j))){
                    ret.push(str.substring(i, j+1));
                }
            }
        }
    }
    ret.sort();
    return ret;
}

function getConsonantSubstrings(str){
    let ret = [];
    for(let i=0;i<str.length;i++){
        if(isConsonant(str.charAt(i))){
            for(let j=str.length-1;j>=i;j--){
                if(isConsonant(str.charAt(j))){
                    ret.push(str.substring(i, j+1));
                }
            }
        }
    }
    ret.sort();
    return ret;
}

//2) Write a function redundant that takes in a string 'str' and returns a function that returns 'str'.
function redundant(str){
    return redundantFunction(str);
}

//Helper functions below.
function isVowel(letter){
    if((letter == 'a') || (letter == 'e') || (letter == 'i') || (letter == 'o') || (letter == 'u')){
        return true;
    }
    else return false;
}

function isConsonant(letter){
    if(isVowel(letter) == true){
        return false;
    }
    else return true;
}

function redundantFunction(str){
    return str;
}